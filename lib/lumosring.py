# lumosring.py (LumosRing)
# Copyright: 2022 Bradan Lane STUDIO
#
# Open Source License: MIT

"""
This CircuitPython library supports the LumosRing hardware.

Implementation Notes
--------------------
**Hardware:**
    * the LumosRing (available on Tindie) https://www.tindie.com/stores/bradanlane/

**Software and Dependencies:**
    * CircuitPython firmware (UF2): https://circuitpython.org/board/lolin_s2_mini/
    * Adafruit CircuitPython Libraries: https://circuitpython.org/libraries
"""

import supervisor
import board
import time
import digitalio
import pwmio
import keypad as keypad_module
import neopixel
import ssl
import wifi
import socketpool

"""
    The wifi credentials and Adafruit IO credentials are stored is a file called secrets.py
    A template of this file is included with the project.
"""
try:
    from secrets import secrets
except ImportError:
    secrets = None
    pass

try:
    from adafruit_datetime import datetime, timedelta
except ImportError as e:
    print("------------------------------------------------------------------------")
    print(e)
    print("------------------------------------------------------------------------")
    print ("Error: Missing 'lib/'adafruit_datetime.pmy'")
    print ("The Adafruit CircuitPython Libraries bundle is available from:")
    print ("https://circuitpython.org/libraries")
try:
    import adafruit_requests
except ImportError as e:
    print("------------------------------------------------------------------------")
    print(e)
    print("------------------------------------------------------------------------")
    print ("Error: Missing 'lib/'adafruit_requests.pmy'")
    print ("The Adafruit CircuitPython Libraries bundle is available from:")
    print ("https://circuitpython.org/libraries")
try:
    from adafruit_bitmap_font import bitmap_font
except ImportError:
    print("------------------------------------------------------------------------")
    print ("Error: Missing 'lib/adafruit_bitmap_font'")
    print ("The Adafruit CircuitPython Libraries bundle is available from:")
    print ("https://circuitpython.org/libraries")


print ("")

class _Buttons:

    """
        the Buttons class manages both buttons
        Buttons support two states: PRESSED and RELEASED
    """

    def __init__(self):
        self._buttons = keypad_module.Keys((board.IO11,board.IO12,), value_when_pressed=False, pull=True)
        self.event = keypad_module.Event()
        # https://docs.circuitpython.org/en/latest/shared-bindings/keypad/index.html
        # event has 'key_number' of 0 or 1, 'pressed' is True or False, also has timestamp

    def has_event(self):
        return self._buttons.events.get_into(self.event)

class _Buzzer:
    """
        The LumosRing has a simple piezo buzzer available for tones.
        Tones are generated using PWM.
        To generate a tone, provide a frequency, and a duration.

        example:
            tone(880, 0.5)
    """

    def __init__(self):
        _STACKSIZE = 6
        self._buzzer_power = digitalio.DigitalInOut(board.IO9)
        self._buzzer_power.direction = digitalio.Direction.OUTPUT
        self._buzzer_power.value = False
        self._pwm = None
        self._timer = 0


    def toneon(self, frequency=880):
        if self._pwm is None:
            self._buzzer_power.value = True
            self._pwm = pwmio.PWMOut(board.IO18, frequency=int(frequency), variable_frequency=True)
            self._pwm.duty_cycle = 0x8000
        else:
            self._pwm.frequency = frequency


    def toneoff(self):
        if self._pwm is not None:
            self._pwm.deinit()
            self._pwm = None
            self._buzzer_power.value = False


    def tone(self, frequency=880, seconds=0.5, wait=True):
        self.toneon(frequency)        
        if wait:
            time.sleep(seconds)
            self.toneoff()
        else:
            self._timer = supervisor.ticks_ms() + int(1000 * seconds)


    def update(self):
        if self._timer:
            if self._timer < supervisor.ticks_ms():
                self._timer = 0
                self.toneoff()


class _Neopixels:
    """
        The LumosRing has a large number of WS2812C-2020 addressable LEDs.
        These LEDs are often referred to as NeoPixels.
        The are connected in two sequences. The outer ring if LEDs is 240.
        The inner two blocks are 35 each for a total of 70.
        The outer circle of LEDs are arranged in radials of 4 LEDs each.
        The first radial consists of LEDs 0..3, the second radial is 4,,7, etc.
        The blocks of LEDs in the center are arranged in rows.
        The top row of the left block consists of LEDs 0..4.
        The second row of the left block consists of LEDs 5..9. etc.
        The top row of the right block consists of LEDs 30..34.
    """
    """
        source of many fonts: https://github.com/olikraus/u8g2/tree/master/tools/font/bdf
        some fonts may cause an error. the most common two problems are:
            1) there 'copyright' text
            2) the bounding box is in the form of a string rather than four values
        BDF font format may be converted to PCF format using the following online tool:
            https://adafruit.github.io/web-bdftopcf/
    """


    def __init__(self):
        try:
            fontname = 'font/5x7.pcf'
            self._font = bitmap_font.load_font(fontname)
        except:
            print("Warning: Missing '%s'" % (fontname))
            print("The font is located in the project 'font' folder on gitlab")
            self._font = None
            pass

        self._brightness = 0.3
        self._ring = neopixel.NeoPixel(board.IO16, 240, brightness=self._brightness, auto_write=False)
        self.ring_fill(LumosRing.BLACK)
        self._ring_dirty = True

        """
            the developmet PCB hs the blocks run 5x7 + 5x7 = 5x14
            the production PCB needs the block to run 10x7
            also, board.IO33 will become board.IO17 for production
        """
        self._block = neopixel.NeoPixel(board.IO17, 70, brightness=self._brightness, auto_write=False)
        self.block_fill(LumosRing.BLACK)
        self._block_dirty = True
        self.show()

    @property
    def direct_access(self, segment=0):
        if segment == LumosRing.RING:
            return self._ring
        return self._block
    @property
    def brightness(self):
        return self._brightness
    @brightness.setter
    def brightness(self, level):
        level = level if level > 0.01 else 0.01
        level = level if level < 0.75 else 0.75
        level = round(level, 2)
        self._brightness = level
        self._block.brightness = level
        self._ring.brightness = level
        self._ring_dirty = True
        self._block_dirty = True

    def ring_fill(self, color):
        self._ring.fill(color)
        self._ring_dirty = True
    def block_fill(self, color):
        self._block.fill(color)
        self._block_dirty = True

    def ring_pixel(self, x, y, color):
        self._ring[(y*4)+x] = color
        self._ring_dirty = True
    def block_pixel(self, x, y, color):
        self._block[(y*5)+x] = color
        self._block_dirty = True

    def ring_hline(self, y, count, color):
        count = count if (count > 0) else 4   # make lines default to full length on ring
        for i in range(count):
            self._ring[(y*4)+i] = color
        self._ring_dirty = True

    def block_image(self, image, color=0x3f3f3f, background=0x000000):
        """
            the developmet PCB hs the blocks run 5x7 + 5x7 = 5x14
                = (39, 38, 37, 36, 35, 4, 3, 2, 1, 0)
            the production PCB needs the block to run 10x7
                = (9, 8, 7, 6, 5, 4, 3, 2, 1, 0)
        """
        # initially we treat 'image' as an array of ints where each int is a line of the bitmap for a monochrome image
        size = len(image)
        #print ("image[%d] = %s" % (size, repr(image)))
        # for each integer in the array
        for i in range(size):
            bits = image[i]
            # the bits representing the 10 leds across
            for j in (9, 8, 7, 6, 5, 4, 3, 2, 1, 0):
                pixel = (i*10) + j
                self._block[pixel] = (color if (bits & 0x1) else background)
                bits = (bits >> 1)
        self._block_dirty = True

    def _render_glyph(self, offset, val, glyph, color, background):
        #print("char=%s" % (chr(val)), glyph)    # debugging
        for col in range(5):
            row = 6
            while (glyph.height <= row):
                for r in (0, 10, 20, 30, 40, 50, 60):
                    self._block[r + (offset + col)] = background
                row -= 1
            for r in (60, 50, 40, 30, 20 ,10 ,0):
                if (col < glyph.width):
                    if (row < glyph.height):
                        bit = glyph.bitmap[col, row]
                        self._block[r + (offset + col)] = (color if (bit & 0x1) else background)
                    else:
                        self._block[r + (offset + col)] = background
                    row -= 1
                    if row < 0:
                        break
                else:
                    self._block[r + (offset + col)] = background


    def block_numbers(self, number, color=0x3f3f3f, background=0x000000):
        if self._font is None:
            return

        number = number % 100
        # for each digit
        for position in (5, 0):
            n = number % 10
            n += 48
            glyph = self._font.get_glyph(n)
            if not glyph:
                continue
            self._render_glyph(position, n, glyph, color, background)
            number = int(number / 10)
        self._block_dirty = True

    def block_letters(self, text, color=0x3f3f3f, background=0x000000):
        if self._font is None:
            return

        text_array = list(text)
        index = 0
        # for each character
        for position in (0, 5):
            n = ord(text_array[index])
            glyph = self._font.get_glyph(n)
            if not glyph:
                continue
            self._render_glyph(position, n, glyph, color, background)
            index += 1
        self._block_dirty = True


    def update(self):
        pass    # currently nothing to do 

    def show(self):
        if self._ring_dirty:
            self._ring.show()
            self._ring_dirty = False
        if self._block_dirty:
            self._block.show()
            self._block_dirty = False


class _Clock:
    NOCLOCK = 0
    CLOCK = 1
    TIMER = 2
    COUNTDOWN = 3

    _ONE_SECOND = timedelta(seconds=1)   # a constant used for updating the clock
    _MAX_ATTEMPTS = 6

    def __init__(self, mode=NOCLOCK):

        self._datetime = datetime(1970, 1, 1, 0, 0, 0)
        self._time = time.time()

        self._synched = False
        self._sync_attempts = 0
        self._dirty = True
        self._tick_interval = 16666666
        self._tick_time = time.monotonic_ns()
        self._tick_timer = self._tick_time

        # public properties
        self.stopped = False if mode == _Clock.CLOCK else True
        self.hour = self._datetime.hour
        self.minute = self._datetime.minute
        self.second = self._datetime.second
        self.tick = 0
        self.tick_enabled = True
        self.mode = mode
        self.manual = False if mode == _Clock.CLOCK else True

        self.resync()
        self.tick_reset()


    def resync(self):
        if self.manual or (self.mode != _Clock.CLOCK):
            return

        print ("Updating time: %2d:%2d:%2d" % (self.hour, self.minute, self.second))
        success = True
        if secrets is None:
            print("Error: Missing 'secrets.py'")
            print("    this file contains the WiFi and Adafruit IO credentials")
            print("    copy secrets_empty.py to secrets.py")
            print("    edit the JSON with your information")
            print("    then copy secrets.py to the 'CIRCUITPY' device")
            print("    for more information on Adafruit IO")
            print("     visit: https://io.adafruit.com")
            success = False

        if success:
            ssid = secrets.get("ssid", None)
            pw = secrets.get("password", None)
            username = secrets.get("aio_username", None)
            key = secrets.get("aio_key", None)

            if ssid is None:
                print("Error: missing 'ssid' in 'secrets.py' file.")
                success = False
            if username is None:
                print("Error: missing 'aio_username' in 'secrets.py' file.")
                success = False
            if key is None:
                # missing secrest.py file or required fields
                # set a generic time and mark it as synced since we will never get a real clock
                print("Error: missing 'aio_username' in 'secrets.py' file.")
                success = False

        if not success:
            self._datetime = datetime(1970, 1, 1, 0, 0, 0)
            self._synched = False    # if we can't sync then we won't try again
            self.manual = True
            return
            
        # returns a datetime object
        wifi.radio.enabled = True
        #print("Connecting to %s"%secrets["ssid"])

        try:
            wifi.radio.connect(secrets["ssid"], secrets["password"])
        except ConnectionError:
            print("Unable to connect to '%s'" % secrets["ssid"])
            self._sync_attempts += 1
            if (not self._synched) or (self._sync_attempts > _Clock._MAX_ATTEMPTS):
                self._datetime = datetime(1970, 1, 1, 0, 0, 0)
                self._synched = False
                self.manual = True
                return

        #print("Connected  to %s" % secrets["ssid"])
        #print("My IP address is", wifi.radio.ipv4_address)

        pool = socketpool.SocketPool(wifi.radio)
        requests = adafruit_requests.Session(pool, ssl.create_default_context())


        TIME_URL = "https://io.adafruit.com/api/v2/%s/integrations/time/strftime?x-aio-key=%s" % (username, key)
        TIME_URL  += "&fmt=%25Y-%25m-%25d+%25H%3A%25M%3A%25S"

        location = secrets.get("timezone", None)
        if location:
            TIME_URL += "&tz=%s" % (location)

        response = requests.get(TIME_URL)
        wifi.radio.enabled = False

        try:
            self._datetime = datetime.fromisoformat(response.text)
            self._synched = True
            self._sync_attempts  = 0
        except ValueError:
            print("Error: Unable to parse response from Adafruit IO service:")
            print("       Service URL: %s" % (TIME_URL))
            print("       Response: %s" % (response.text))

            self._sync_attempts += 1
            if (not self._synched) or (self._sync_attempts > _Clock._MAX_ATTEMPTS):
                self._datetime = datetime(1970, 1, 1, 0, 0, 0)
                self._synched = False
                self.manual = True
                return

        # end of our single-pass loop
        self._time = time.time()
        self.hour = self._datetime.hour
        self.minute = self._datetime.minute
        self.second = self._datetime.second
        self.tick = 0
        print ("Time updated:  %2d:%2d:%2d" % (self.hour, self.minute, self.second))


    def tick_reset(self):
        if self.tick < 58:
            #print("tick slow: %2d  %d %d" % (self.tick, self._tick_interval, (time.monotonic_ns() - (self._tick_timer - self._tick_interval))))
            self._tick_interval -= 166666
        if self.tick > 59:
            #print("tick fast: %2d  %d %d" % (self.tick, self._tick_interval, (time.monotonic_ns() - (self._tick_timer - self._tick_interval))))
            self._tick_interval += 83333
        # some dynamic recalibration
        self._tick_time = time.monotonic_ns()
        self._tick_timer = self._tick_time
        self.tick = 0


    def update(self):
        if self._datetime is None:
            return
        if self.stopped == True:
            return
        # if countdown and we are at 0:0:0
        if self.mode == _Clock.COUNTDOWN:
            if (not self.hour) and (not self.minute) and (not self.second):
                self.stopped = True
                return

        if self.tick_enabled:
            self._tick_time = time.monotonic_ns()
            if self._tick_timer <= self._tick_time:
                self._tick_timer = self._tick_time + self._tick_interval
                self.tick += 1
                self._dirty = True
        # end of ticker

        if self._time != time.time():
            self._dirty = True
            self._time = time.time()
            self._datetime += _Clock._ONE_SECOND
            self.hour = self._datetime.hour
            self.minute = self._datetime.minute
            self.second = self._datetime.second

            self.tick_reset()

            if self.second == 0:
                if self.minute == 0:
                    # at 4, 12, 20 hours, we resync the clock with the internet
                    if (self.hour == 4) or (self.hour == 12) or (self.hour == 20):
                        self.resync()
                else:
                    # if we failed our last sync attempt, then we will try every 5 minutes for up to MAX_ATTEMPTS
                    if not self.manual:
                        if (self.minute % 5) and (not self._synched) and (self._sync_attempts <= _Clock._MAX_ATTEMPTS):
                            self.resync()


class LumosRing:

    # clock modes
    NOCLOCK = 0
    CLOCK = 1
    TIMER = 2
    COUNTDOWN = 3

    # time increments
    ONESECOND = timedelta(seconds=1)   # a constant used for updating the clock
    ONEMINUTE = timedelta(minutes=1)   # a constant used for updating the clock
    ONEHOUR = timedelta(hours=1)   # a constant used for updating the clock

    # buttons
    LEFT = 0
    RIGHT = 1

    # button states
    RELEASED = 0
    PRESSED = 1
    
    # bank of LEDs
    RING = 0
    BLOCK = 1

    RED		= 0xff0000 #(255,  0,  0)
    GREEN	= 0x007f00 #(  0,127,  0)
    ORANGE  = 0xe04020 #(255, 93,  0)
    YELLOW	= 0xffff00 #(255,127,  0)
    BLUE	= 0x0000ff #(  0, 0, 255)
    MAGENTA	= 0xff00ff #(255,  0,255)
    PURPLE	= 0x3f007f #( 63,  0,127)
    PINK	= 0x7f007f #(127,  0,127)
    CYAN	= 0x00ffff #(  0,127,255)
    WHITE	= 0x7f7f7f #(127,127,127)
    GRAY	= 0x1f1f1f #( 31, 31, 31)
    GREY = GRAY
    DKGRAY	= 0x0f0f0f #( 15, 15, 15)
    BLACK	= 0x000000 #(  0,  0,  0)

    def __init__(self, mode=NOCLOCK):
        self._buzzer = _Buzzer()
        self._buttons = _Buttons()
        self._leds = _Neopixels()
        self._clock = _Clock(mode)

        self._markers = True
        self._background = LumosRing.BLACK
        # hours minutes, seconds, ticks, marks
        self._colors = [LumosRing.RED, LumosRing.GREEN, LumosRing.BLUE, LumosRing.YELLOW, LumosRing.DKGRAY]

        if (self._clock.mode == LumosRing.CLOCK) and self._clock.manual:
            # in manual mode, we give the user the chance to use the buttons to set the clock
            self.tone (523, 0.1)
            self.tone (494, 0.1)
            self.tone (523, 0.2)
            self._set_clock()


    def _set_clock(self):
        timer = supervisor.ticks_ms() + 15000 #  seconds to set the time manually
        left_timer = 0
        right_timer = 0
        while timer > supervisor.ticks_ms():
            do_left = False
            do_right = False
            if self.buttons_changed:
                button, action = self.button
                if button == LumosRing.LEFT:
                    if  action == True:
                        do_left = True
                        left_timer = supervisor.ticks_ms() + 750
                    else:
                        left_timer = 0
                if button == LumosRing.RIGHT:
                    if action == True:
                        do_right = True
                        right_timer = supervisor.ticks_ms() + 750
                    else:
                        right_timer = 0

            # support press-and-hold
            if left_timer and (left_timer < supervisor.ticks_ms()):
                do_left = True
                left_timer = supervisor.ticks_ms() + 125
            if right_timer and (right_timer < supervisor.ticks_ms()):
                do_right = True
                right_timer = supervisor.ticks_ms() + 125

            if do_left:
                if self._clock.mode == LumosRing.COUNTDOWN:
                    self._clock.hour -= 1
                    if self._clock.hour < 0:
                        self._clock.hour += 24
                else:
                    self._clock.hour += 1
                self._clock.hour = self._clock.hour % 24
                self._clock._dirty = True
                timer = supervisor.ticks_ms() + 10000 #  seconds to set the time manually
            if do_right:
                # left button press. advance hour
                if self._clock.mode == LumosRing.COUNTDOWN:
                    self._clock.minute -= 1
                    if self._clock.minute < 0:
                        self._clock.minute += 60
                else:
                    self._clock.minute += 1
                self._clock.minute = self._clock.minute % 60
                self._clock._dirty = True
                timer = supervisor.ticks_ms() + 10000 #  seconds to set the time manually

            if self._clock._dirty:
                print ("Time: %2d:%2d:%2d" % (self._clock.hour, self._clock.minute, self._clock.second))
                self._show()

        self._clock._datetime = datetime(1970, 1, 1, self._clock.hour, self._clock.minute, 0)
        # end of _set_clock()


    def _show(self):
        #start = supervisor.ticks_ms()
        if (self._clock.mode != _Clock.NOCLOCK) and self._clock._dirty:
            self._clock._dirty = False
            #print ("%02d:%02d:%02d" % (self._clock.hour, self._clock.minute, self._clock.second))
            self._leds.ring_fill(self._background) # this wipes the entire string, we only want to wipe the first 240

            # optionally render markings around dial
            if self._markers:
                color = self._colors[4] # colors are : hour, minute, seconds, ticks, marks
                for i in range(0, 60, 5):
                    self._leds.ring_pixel(3, i, color)
                    if (i % 3) == 0:
                        self._leds.ring_pixel(2, i, color)
            """
                render minutes before hours so a portion of each is visible when they overlap
                then render seconds and finally the tick (if enabled)

                the clock face only has 12 hours
                each hour takes 5 positions around the face
                we increment within a 5-position slot proportional to the number of minutes past the hour

                one more oddity: the '0' at top dead center is actually #59
            """

            color = self._colors[1] # colors are : hour, minute, seconds, ticks, marks
            minute = self._clock.minute
            # minute = (minute - 1) if (minute > 0) else 59
            self._leds.ring_hline(minute, 4, color)

            if not ((self._clock.mode == LumosRing.COUNTDOWN) and ((self._clock.hour == 0) or self._clock.hour == 23)):
                color = self._colors[0] # colors are : hour, minute, seconds, ticks, marks

                hour = self._clock.hour if (self._clock.hour < 12) else (self._clock.hour - 12)
                hour = (hour * 5) + int(self._clock.minute/12)
                # hour = (hour - 1) if (hour > 0) else 59
                self._leds.ring_hline(hour, 3, color)


            color = self._colors[2] # colors are : hour, minute, seconds, ticks, marks
            second = self._clock.second
            # second = (second - 1) if (second > 0) else 59
            self._leds.ring_pixel(3, second, color)

            # WARNING: can not do both tracer and markers fast enough
            if self._clock.tick_enabled and (not self._markers):
                color = self._colors[3] # colors are : hour, minute, seconds, ticks, marks
                tick = self._clock.tick
                # tick = (tick - 1) if (tick > 0) else 59
                if tick < 60:
                    self._leds.ring_pixel(3, tick, color)
                
        self._leds.show()
        #end = supervisor.ticks_ms()
        #if end - start > 5:
        #    print("slow show", end-start)

    def update(self):
        #start = supervisor.ticks_ms()
        self._buzzer.update()
        if self._clock.mode != LumosRing.NOCLOCK:
            self._clock.update()
        self._show()
        #end = supervisor.ticks_ms()
        #if end - start > 5:
        #    print("slow update", end-start)

    @property
    def mode(self):
        return self._clock.mode
    @mode.setter
    def mode(self, mode):
        if mode == self._clock.mode:
            return
        old_mode = self._clock.mode
        self._clock.mode = mode
        if mode == LumosRing.CLOCK:
            self._clock.resync()
            if self._clock.manual:
                self.set_clock()

    @property
    def clock_background(self):
        return self._background
    @clock_background.setter
    def clock_background(self, color):
        self._background = color
        self._clock._dirty = True
    @property
    def clock_colors(self):
        return self._colors
    @clock_colors.setter
    def clock_colors(self, colors):
        if isinstance(colors, list):
            size = len(self._colors)
            # init all colors to gray
            for i in range(size):
                self._colors[i] = LumosRing.GRAY
            size = len(colors)
            size = size if size < 5 else 5
            for i in range(size):
                self._colors[i] = colors[i]
        # if a single color was provided, set all colors to that value
        if isinstance(colors, int):
            size = len(self._colors)
            for i in range(size):
                self._colors[i] = colors
            self._clock._dirty = True
    @property
    def brightness(self):
        return self._leds.brightness
    @brightness.setter
    def brightness(self, level):
        self._leds.brightness = level

    @property
    def ticker(self):
        return self._clock.tick_enabled
    @ticker.setter
    def ticker(self, enabled):
        if self._markers and enabled:
            print("WARNING: using clock markers with clock ticks is too slow. Disabling markers.")
            self._markers = False
        self._clock.tick_enabled = enabled
    @property
    def markers(self):
        return self._markers
    @markers.setter
    def markers(self, enabled):
        if self._clock.tick_enabled and enabled:
            print("WARNING: using clock markers with clock ticks is too slow. Disabling ticks.")
            self._clock.tick_enabled = False
        self._markers = enabled

    @property
    def time(self):
        return self._clock._datetime
    @time.setter
    def time(self, dt):
        if isinstance(dt, datetime):
            self._clock._datetime = dt
        else:
            h = 0
            m = 0
            s = 0
            if isinstance(dt, tuple):
                if len(dt) > 0:
                    h = abs(dt[0])
                if len(dt) > 1:
                    m = abs(dt[1])
                if len(dt) > 2:
                    s = abs(dt[2])
            elif isinstance(dt, int):
                h = int(dt / 3600)
                dt = dt % 3600
                m = int(dt / 60)
                dt = dt % 60
                s = dt
            else:
                print("Error: unknown data type assigned to LumosRing.time")

            h = h if h < 24 else 0
            m = m if m < 60 else 0
            s = s if s < 60 else 0
            self._clock._datetime = datetime(1970, 1, 1, h, m, s)
        # end input types
        self._clock.hour = self._clock._datetime.hour
        self._clock.minute = self._clock._datetime.minute
        self._clock.second = self._clock._datetime.second
        self._clock._dirty = True
    @property
    def hour(self):
        return self._clock.hour
    @hour.setter
    def hour(self, h):
        self._clock.hour = h
        self._clock._datetime = datetime(1970, 1, 1, self._clock.hour, self._clock.minute, 0)
        self._clock._dirty = True
    @property
    def minute(self):
        return self._clock.minute
    @minute.setter
    def minute(self, m):
        self._clock.minute = m
        self._clock._datetime = datetime(1970, 1, 1, self._clock.hour, self._clock.minute, 0)
        self._clock._dirty = True
    @property
    def second(self):
        return self._clock.second
    @second.setter
    def second(self, s):
        self._clock.second = s
        self._clock._datetime = datetime(1970, 1, 1, self._clock.hour, self._clock.minute, self._clock.second)
        self._clock._dirty = True

    @property
    def stopped(self):
        return self._clock.stopped
    def stop(self):
        self._clock.stopped = True
    def start(self):
        self._clock.stopped = False
        
    @property
    def direct_access(self):
        return self._leds.direct_access
    def leds(self, color):
        self._leds.ring_fill(color)
    def led(self, num, color):
        if isinstance(num, int):
            self._leds.ring_pixel(num%4, int(num/4), color)
        elif isinstance(num, tuple):
            if len(num) == 2:
                self._leds.ring_pixel(num[0], num[1], color)
            else:
                print("Error: unable to identify LumosRing.led number '", num, "'.")
        else:
            print("Error: unknown data type for LumosRing.led")
    def line(self, num, color):
        row = 0
        count = 0
        if isinstance(num, int):
            row = num
        elif isinstance(num, tuple):
            if len(num) == 2:
                row = num[0]
                count = num[1]
            else:
                print("Error: unable to identify LumosRing.line number '", num, "'.")
        else:
            print("Error: unknown data type for LumosRing.line")
        self._leds.ring_hline(row, count, color)
    def bitmap(self, image, color=GRAY, background=BLACK):
        self._leds.block_image(image, color, background)
    def digits(self, number, color=GRAY, background=BLACK):
        self._leds.block_numbers(number, color, background)
    def letters(self, text, color=GRAY, background=BLACK):
        self._leds.block_letters(text, color, background)

    def tone(self, freq, len):
        self._buzzer.tone(freq, len)
    def toneon(self, freq):
        self._buzzer.toneon(freq)
    def toneoff(self):
        self._buzzer.toneoff()

    @property
    def buttons_changed(self): # returns a boolean True if there is new event data
        return self._buttons.has_event()
    @property
    def button(self):
        return self._buttons.event.key_number, self._buttons.event.pressed
