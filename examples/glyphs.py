# code.py (LumosRing Metronome example)
# Copyright: 2022 Bradan Lane STUDIO
#
# Open Source License: MIT

"""
This CircuitPython program demonstrates the basics of the LumosRing LEDs, buttons, and tone buzzer.

The LumosRing hardware connects via USB and provides:
    - 310 Neopixel LEDs
        - ring of 240 LEDs as 60 lines of 4 LEDs each
        - block of 10x7 LEDs as 7 lines of 10 LEDs each
    - tone buzzer
    - 2 buttons
    - a small mass storage device (used to store the code and related files)

The LumosRing is powered by a Lolin ESP32-S2 which include Wifi connectivity.

Documentation: 
**Hardware:**
    * the LumosRing (available on Tindie)
      https://www.tindie.com/stores/bradanlane/
"""

import supervisor

try:
    from LumosRing import LumosRing   # REQUIRED: import the LumosRing library
except ImportError as e:
    print("------------------------------------------------------------------------")
    print(e)
    print("------------------------------------------------------------------------")
    print ("Error: Missing 'lib/LumosRing.py'")
    print ("The latest LumosRing library is available from:")
    print ("https://gitlab.com/bradanlane/circuitpython-projects/-/tree/main/LumosRing")
    print("------------------------------------------------------------------------")


lumos = LumosRing(mode=LumosRing.NOCLOCK)
lumos.brightness = 0.15

text = "LR"
lumos.letters(text)

index = -2

while True:
    lumos.update()
    while lumos.buttons_changed:
        button, action = lumos.button
        if (button == LumosRing.LEFT) and (action == LumosRing.PRESSED):
            index = (index - 2) if index > 0 else 126
            #lumos.letters(chr(index + 65) + chr(index + 97), 0x7f00ff)
            lumos.letters(chr(index) + chr(index + 1), 0x7f00ff)
        if (button == LumosRing.RIGHT) and (action == LumosRing.PRESSED):
            index = (index + 2) if index < 126 else 0
            #lumos.letters(chr(index + 65) + chr(index + 97), 0x1f3f00)
            lumos.letters(chr(index) + chr(index + 1), 0x1f3f00)

# end of while True loop
