# code.py (LumosRing Metronome example)
# Copyright: 2022 Bradan Lane STUDIO
#
# Open Source License: MIT

"""
This CircuitPython program demonstrates the basics of the LumosRing LEDs, buttons, and tone buzzer.

The LumosRing hardware connects via USB and provides:
    - 310 Neopixel LEDs
        - ring of 240 LEDs as 60 lines of 4 LEDs each
        - block of 10x7 LEDs as 7 lines of 10 LEDs each
    - tone buzzer
    - 2 buttons
    - a small mass storage device (used to store the code and related files)

The LumosRing is powered by a Lolin ESP32-S2 which include Wifi connectivity.

Documentation: 
**Hardware:**
    * the LumosRing (available on Tindie)
      https://www.tindie.com/stores/bradanlane/
"""

import supervisor

try:
    from LumosRing import LumosRing   # REQUIRED: import the LumosRing library
except ImportError as e:
    print("------------------------------------------------------------------------")
    print(e)
    print("------------------------------------------------------------------------")
    print ("Error: Missing 'lib/LumosRing.py'")
    print ("The latest LumosRing library is available from:")
    print ("https://gitlab.com/bradanlane/circuitpython-projects/-/tree/main/LumosRing")
    print("------------------------------------------------------------------------")


class Metronome:
    MIN_BPM = 30
    MAX_BPM = 200
    SINE120 = [0,275,547,813,1069,1315,1545,1759,1954,2127,2277,2402,2501,2572,2615,2629]  # far from accurate across the BPM range

    ENDSTOP = 16

    def __init__(self, ring):
        self.lumos = ring
        self._bpm = 90
        self._slice = 0
        self._direction = 1
        self._speed = 0
        self._position = 0
        self.sleep_duration = 0

    def increment(self):
        # avoid doubling up at the end points
        if not (self._slice % Metronome.ENDSTOP):
            self._slice += 1

        slice = self._slice
        self._direction = 1

        if slice > (Metronome.ENDSTOP * 2):
            self._direction = -1
            slice -= (Metronome.ENDSTOP * 2)
        if slice > Metronome.ENDSTOP:
            slice = (Metronome.ENDSTOP * 2) - slice
            slice -= 1
        self._speed = Metronome.SINE120[slice]
        self._position = slice * self._direction
        #print ("update[%2d]: %2d  %-2d %4d" % (self._slice, slice, self._position, self._speed))

        self._slice +=1
        if self._slice >= (Metronome.ENDSTOP * 4):
            self._slice = 0

    def sleep(self, duration):
        if duration:
            timer = supervisor.ticks_ms() + duration
            while timer > supervisor.ticks_ms():
                continue

    def update(self):
        start = supervisor.ticks_ms()
        # we randomize eye movement (front is always between other positions)
        self.increment()
        duration = self.delay
        num = self.position if self.position >= 0 else 60 + self.position
        if (self.position == 15) or (self.position == -15):
            duration -= (supervisor.ticks_ms() - start)
            #print("arm: %+3d %d" % (self.position, duration))
            duration = duration if duration > 40 else 40
            self.lumos.toneon(880)
            self.sleep(duration)
            self.lumos.toneoff()
        else:
            self.lumos.leds(0x000000)
            self.lumos.line(num, 0x5f1f00)
            self.lumos.update()
            duration -= (supervisor.ticks_ms() - start)
            self.sleep(duration)

    @property
    def delay(self):
        delay = int( (self._speed + (self._bpm - 1)) /  self._bpm )
        delay = delay if delay else 1
        return delay
    @property
    def direction(self):
        return self._direction
    @property
    def position(self):
        return self._position
    @property
    def bpm(self):
        return self._bpm
    @bpm.setter
    def bpm(self, val):
        #print ("BPM: %d %d" % (val, self._bpm))
        if val < Metronome.MIN_BPM:
            return
        if val > Metronome.MAX_BPM:
            return
        self._bpm = val

lumos = LumosRing(mode=LumosRing.NOCLOCK)
lumos.tone(220,0.1)
met = Metronome(lumos)
lumos.digits(met.bpm, 0x0f7f0f)

while True:
    met.update()
    lumos.update()
    while lumos.buttons_changed:
        button, action = lumos.button
        if (button == LumosRing.LEFT) and (action == LumosRing.PRESSED):
            met.bpm -=5
            lumos.digits(met.bpm, 0x0f7f0f)
        if (button == LumosRing.RIGHT) and (action == LumosRing.PRESSED):
            met.bpm +=5
            lumos.digits(met.bpm, 0x0f7f0f)

# end of while True loop
