# code.py (LumosRing Clock example)
# Copyright: 2022 Bradan Lane STUDIO
#
# Open Source License: MIT

"""
This CircuitPython program demonstrates the basics of the LumosRing LEDs, buttons, and tone buzzer.

The LumosRing hardware connects via USB and provides:
    - 310 Neopixel LEDs
        - ring of 240 LEDs as 60 lines of 4 LEDs each
        - block of 10x7 LEDs as 7 lines of 10 LEDs each
    - tone buzzer
    - 2 buttons
    - a small mass storage device (used to store the code and related files)

The LumosRing is powered by a Lolin ESP32-S2 which include Wifi connectivity.

This sample implements a basic Chess Match timer:

    - The LumosRing start and displays '--" to indicate new match.
    - The buttons are used to set the number of minutes each player gets for the match.
    - Once the buttons are left untouched for 5 seconds, the timer is set and waits to begin.
    - The first player touches their button to begin.
    - When finished they touch their button again which swaps to timer to the other player.
    - When that player presses their button, the timer switched players again.
    - This continues until a player concedes the match by pressing both buttons or
      one of the players runs out of time.
    - Pressing either button starts the whole process again.

Documentation: 
**Hardware:**
    * the LumosRing (available on Tindie)
      https://www.tindie.com/stores/bradanlane/
"""

import supervisor
import random
import time


try:
    from LumosRing import LumosRing   # REQUIRED: import the LumosRing library
except ImportError as e:
    print("------------------------------------------------------------------------")
    print(e)
    print("------------------------------------------------------------------------")
    print ("Error: Missing 'lib/LumosRing.py'")
    print ("The latest LumosRing library is available from:")
    print ("https://gitlab.com/bradanlane/circuitpython-projects/-/tree/main/LumosRing")
    print("------------------------------------------------------------------------")

lumos = LumosRing(mode=LumosRing.COUNTDOWN)
lumos.ticker = False
lumos.clock_colors = [LumosRing.WHITE, LumosRing.WHITE, LumosRing.WHITE, LumosRing.WHITE, LumosRing.GRAY]
lumos.letters("--")

lumos.time = (0,0,0)
player1 = lumos.time
player2 = lumos.time
player1_color = 0x00003f
player2_color = 0x270f00

game_state = 0 # 0 is new, 1 is ready, 2 is active, 3 is conceding , 4 is finished
player = 0
timer = supervisor.ticks_ms() + 5000 # 5 seconds to start setting the countdown timer

# remember buttons
left = False
right = False

while True:
    lumos.update()

    if lumos.buttons_changed:
        button, action = lumos.button
        left = action if button == LumosRing.LEFT else left
        right = action if button == LumosRing.RIGHT else right
        #print("main: ", lumos._buttons.event, game_state, left, right)
    else:
        action = False

    if game_state == 0:
        # now the user can use the buttons for up/down to set the timer
        if timer > supervisor.ticks_ms():
            if (action):
                if button == LumosRing.LEFT:
                    lumos.time += LumosRing.ONEMINUTE
                if button == LumosRing.RIGHT:
                    lumos.time -= LumosRing.ONEMINUTE
                lumos.digits (60 - lumos.minute)
                timer = supervisor.ticks_ms() + 5000 # 5 seconds to continue setting the countdown timer
        elif lumos.minute == 0:
            timer = supervisor.ticks_ms() + 5000 # 5 seconds to start setting the countdown timer
        else:
            #print("timer set to %02d:%02d:%02d" % (lumos.hour, lumos.minute, lumos.second))
            # set both players to have the same amount of time
            player1 = lumos.time
            player2 = lumos.time
            lumos.letters("GO")
            game_state = 1
    # end of setting up timer for new match

    if game_state > 0:
        if left and right:
            # a player wants to end the game
            lumos.toneon(265)
            game_state = 3

    if game_state == 1:
        # the first press starts the player's timer; subsequent presses toggles the timer to the other player
        if action:
            if left:
                player = 1
                lumos.time = player1
                lumos.clock_background = player1_color
                lumos.digits(1, player1_color)
                lumos.start()
                game_state = 2
            if right:
                player = 2
                lumos.time = player2
                lumos.clock_background = player2_color
                lumos.digits(2, player2_color)
                lumos.start()
                game_state = 2
        # end of first move
    elif game_state == 2:
        if action:
            if left:
                if player == 1:
                    player1 = lumos.time
                    player = 2
                    lumos.time = player2
                    lumos.clock_background = player2_color
                    lumos.digits(2, player2_color)
            if right:
                if player == 2:
                    player2 = lumos.time
                    player = 1
                    lumos.time = player1
                    lumos.clock_background = player1_color
                    lumos.digits(1, player1_color)
        # end of switch player
        if lumos.stopped:
            # if the countdown stopped, while the game was active, it means a player ran out of time
            lumos.clock_background = 0x1f0000
            game_state = 4
            lumos.tone(265, 2.0)
        # end of game because time ran out

    elif game_state == 3:
            # wait for the buttons to be released
            if left or right:
                time.sleep(0.1)
            else:
                lumos.stop()
                lumos.toneoff()
                lumos.letters("--")
                lumos.clock_background = 0x1f0000
                game_state = 4
        # end of game because player conceded

    elif game_state == 4:
        if left or right:
            # start a new game
            player = 0
            lumos.time = (0,0,0)
            lumos.letters("--")
            lumos.clock_background = LumosRing.BLACK
            timer = supervisor.ticks_ms() + 5000 # 5 seconds to start setting the countdown timer
            game_state = 0
    # end of user requested to start new match

# end of while True loop
